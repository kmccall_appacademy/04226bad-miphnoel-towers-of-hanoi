
class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from_tower, to_tower)
    from, to = @towers[from_tower], @towers[to_tower]

    raise "invalid move" unless valid_move?(from, to)

    to.push(from.pop)

    move_report(to.last, from_tower, to_tower)
  end

  def valid_move?(from, to)
    !from.empty? && (to.empty? || from.last < to.last)
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end

  def play
    give_introduction

    until won?
      take_turn
    end

    puts "\n\nCongrats! You won!\nPlay again? (y/n)"
    gets.chomp == 'y' ? reset : "Stay sweet, mi amigo"
  end

  def render
    puts "Would you like to play a game of Towers of Hanoi? (y/n)"
    answer = gets.chomp

    case answer
    when 'y'
      play
    when 'n'
      puts "Ok, maybe next time."
    else
      puts "Sorry, I don't understand."
    end
  end

  private

  def give_introduction
    puts "\nIt's time for Towers of Hanoi!"
    puts "To win, move all of the discs from the first tower onto another tower."
    puts "The catch: you cannot move a larger disc onto a smaller disc."
    puts "Have fun!\n\n"
  end

  def status_report
    @towers.each_with_index do |tower, idx|
      if tower.empty?
        puts "Tower #{idx + 1} is empty"
      else
        puts "Tower #{idx + 1} holds disc(s): #{tower.reverse.join(', ')}"
      end
    end
  end

  def take_turn
    status_report
    from_tower = get_move_from
    to_tower = get_move_to
    move(from_tower, to_tower)
  rescue
    puts "Sorry, that is not a valid move.\n\n"
    take_turn
  end

  def get_move_from
    puts "\nWhich tower would you like to move a disc FROM?"
    (gets.chomp).to_i - 1
  end

  def get_move_to
    puts "Which tower would you like to move the disc TO?"
    (gets.chomp).to_i - 1
  end

  def move_report(disc, move_from, move_to)
    puts "\nYou moved disc #{disc} from Tower #{move_from + 1} to Tower #{move_to + 1}\n\n"
  end

  def reset
    @towers = [[3, 2, 1], [], []]
    play
  end
end

game = TowersOfHanoi.new
game.render
